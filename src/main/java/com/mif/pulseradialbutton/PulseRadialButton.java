package com.mif.pulseradialbutton;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Pavel Barmin on 10.08.16
 */
public class PulseRadialButton extends TextView {

    private static final int BITS_COUNT = 3;
    private static final int[] ALPHA_OUT = getAlphaOut();

    private final RectF mDrawableRect = new RectF();

    private final Paint mFillPaint = new Paint();
    private final Paint mRingPaint = new Paint();

    private final long ACTION_TIME_MAX = 1000;

    private float mRingScale = 0.4f;
    private int mFillColor = Color.TRANSPARENT;
    private float mDrawableRadius;
    private float pulseRadius;
    private float pulseRingSize = 0.0f;
    private int currentFrame = 0;
    private boolean isPulsing = false;
    private Runnable animationRunnable = new Runnable() {
        @Override
        public void run() {
            Handler handler = getHandler();
            if (handler != null && isPulsing) {
                handler.removeCallbacks(this);
                invalidate();
            }
        }
    };
    private float bitsPerSecond = 1f;
    private boolean allowPulseOnClick = false;
    private float tracesQuantity;
    private long pulsingRedrawDelay;
    private int minSideLength;
    private long lastActionTime;

    public PulseRadialButton(Context context) {
        super(context);
        init(null);
    }

    public PulseRadialButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public PulseRadialButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PulseRadialButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private static int[] getAlphaOut() {
        int[] alpha = new int[BITS_COUNT];
        final int step = 255 / BITS_COUNT;
        alpha[0] = 255;
        for (int i = 1; i < alpha.length; i++) {
            alpha[i] = alpha[i - 1] - step;
        }

        return alpha;
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.PulseRadialButton);

            mFillColor = a.getColor(R.styleable.PulseRadialButton_fill, mFillColor);
            pulseRingSize = a.getDimension(R.styleable.PulseRadialButton_bit_size, pulseRingSize);
            allowPulseOnClick = a.getBoolean(R.styleable.PulseRadialButton_pulse_onclick, allowPulseOnClick);
            bitsPerSecond = a.getFloat(R.styleable.PulseRadialButton_bps, bitsPerSecond);
            mRingScale = a.getFraction(R.styleable.PulseRadialButton_pulse_size, 1, 1, mRingScale);

            a.recycle();
        }

        setupPaint();

        setBackground(null);
        setGravity(Gravity.CENTER);
        setOnClickListener(null);
    }

    private void setupPaint() {
        mFillPaint.setStyle(Paint.Style.FILL);
        mFillPaint.setAntiAlias(true);
        mFillPaint.setColor(mFillColor);

        mRingPaint.setStyle(Paint.Style.STROKE);
        mRingPaint.setAntiAlias(true);
        mRingPaint.setColor(mFillColor);
        mRingPaint.setStrokeWidth(pulseRingSize);
    }

    private void updateBounds() {
        if (getWidth() == 0 && getHeight() == 0) {
            return;
        }

        mDrawableRect.set(calculateBounds());
        mDrawableRadius = Math.min(mDrawableRect.height() / 2.0f, mDrawableRect.width() / 2.0f);

        setupRingMiscs();

        invalidate();
    }

    private RectF calculateBounds() {
        int availableWidth = getWidth();
        int availableHeight = getHeight();

        minSideLength = Math.min(availableWidth, availableHeight);
        // The actual view should be smaller
        int sideLength = (int) (minSideLength * (1 - mRingScale));

        float left = (availableWidth - sideLength) / 2f;
        float top = (availableHeight - sideLength) / 2f;

        return new RectF(left, top, left + sideLength, top + sideLength);
    }

    private void setupRingMiscs() {
        pulseRadius = mDrawableRect.width() / 2f;
        tracesQuantity = (minSideLength - pulseRadius) / pulseRingSize;
        pulsingRedrawDelay = (long) (1000 / (bitsPerSecond * (tracesQuantity + ALPHA_OUT.length / 2f)));
    }

    @Override
    public void setOnClickListener(final OnClickListener l) {
        super.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allowPulseOnClick) {
                    togglePulsing();
                }
                if (l != null) {
                    l.onClick(view);
                }
            }
        });
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        updateBounds();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        // Make sure to stop animations
        Handler handler = getHandler();
        if (handler != null) {
            handler.removeCallbacks(animationRunnable);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // The base
        canvas.drawCircle(mDrawableRect.centerX(), mDrawableRect.centerY(), mDrawableRadius, mFillPaint);

        super.onDraw(canvas);

        if (isPulsing) {
            // Traces
            float stepRadius = pulseRingSize;
            int drawnTraces = 0;
            int step = -1;
            int alpha;
            int alpha_index = currentFrame;
            int maxAlpha = 0;
            int inIndex = 1;

            while ((drawnTraces < tracesQuantity)
                    && ((mDrawableRadius + stepRadius) * 2 < minSideLength)) {

                if (alpha_index <= 0) {
                    step = 1;
                }

                alpha = alpha_index >= ALPHA_OUT.length ? 0 : ALPHA_OUT[alpha_index];
                if (alpha > maxAlpha) {
                    maxAlpha = alpha;
                }

                // Draw only visible traces
                if (alpha != 0) {
                    float newRadius = mDrawableRadius + stepRadius - mRingPaint.getStrokeWidth() / 2f;
                    if ((mDrawableRadius + stepRadius + pulseRingSize * (ALPHA_OUT.length)) * 2 >= minSideLength) {
                        alpha = (int) (alpha * (1 - inIndex++ / (float) ALPHA_OUT.length));
                    }

                    mRingPaint.setAlpha(alpha);

                    canvas.drawCircle(mDrawableRect.centerX(), mDrawableRect.centerY(),
                            newRadius, mRingPaint);
                }
                stepRadius += pulseRingSize;

                alpha_index += step;

                drawnTraces++;
            }

            currentFrame++;
            if (currentFrame >= tracesQuantity || (drawnTraces < tracesQuantity && maxAlpha == 0)) {
                currentFrame = 0;
            }

            getHandler().postDelayed(animationRunnable, pulsingRedrawDelay);
        } else {
            currentFrame = 0;
            getHandler().removeCallbacks(animationRunnable);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastActionTime = System.currentTimeMillis();
                break;

            case MotionEvent.ACTION_UP:
                if (System.currentTimeMillis() - lastActionTime <= ACTION_TIME_MAX) {
                    if (mDrawableRect.contains(event.getX(), event.getY())) {
                        // Make sure waves are not clickable
                        performClick();
                    }
                    return true;
                }
                break;
        }

        return super.onTouchEvent(event);
    }

    private void togglePulsing() {
        isPulsing = !isPulsing;
        invalidate();
    }

    public void startPulse() {
        isPulsing = true;
        invalidate();
    }

    public void stopPulse() {
        isPulsing = false;
        invalidate();
    }

    public void setFillColor(int color) {
        mFillColor = color;
        updatePaint();

        invalidate();
    }

    private void updatePaint() {
        mFillPaint.setColor(mFillColor);
        mRingPaint.setColor(mFillColor);
    }
}
